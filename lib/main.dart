// ignore_for_file: dead_code

import 'package:flutter/material.dart';

import 'MyPages/Login.dart';

void main() {
  runApp(const MyApp());
}

// whenever your initialization is completed, remove the splash screen:

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.black45,
        hintColor: const Color.fromARGB(255, 124, 33, 243),
        backgroundColor: Colors.black,
      ),
      home: const Login(),
      color: const Color.fromARGB(255, 46, 5, 110),
    );
  }
}
